package main

import (
	"context"
	"fmt"
	"log"
	"priceMonitoring/server/utils"

	"google.golang.org/grpc"
	pb "priceMonitoring/proto"
)

func main() {
	conn, err := grpc.Dial(fmt.Sprintf("localhost:%s", utils.GetConfig().Server.Port), grpc.WithInsecure())
	if err != nil {
		log.Println(err)
	}
	defer conn.Close()

	c := pb.NewPriceMonitoringServiceClient(conn)

	//response, err := c.GetUrlsCheckInfo(context.Background(), &pb.GetUrlsCheckInfoRequest{Url: "http://youtube.com"})
	//response, err := c.AddUrl(context.Background(), &pb.AddUrlRequest{Url: "http://google.com", Periodicity: 5})
	//response, err := c.DisableUrlInfo(context.Background(), &pb.DisableUrlInfoRequest{Url: "http://youtube.com"})
	response, err := c.GetUrlsCheckInfoByDate(context.Background(), &pb.GetUrlsCheckInfoByDateRequest{Date: "2021-05-23 18:26:37.279386 +00:00", N: 10})
	if err != nil {
		log.Println(err.Error())
	}
	fmt.Printf("%v", response)

}
