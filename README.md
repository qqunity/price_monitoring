# PriceMonitoring

[![GoLang](https://secure.meetupstatic.com/photos/member/5/e/e/0/member_272004288.jpeg)](https://golang.org/)

## Installation (local)

PriceMonitoring requires [GoLang](https://golang.org/) v1.16.4 to run.

Install the go module

```sh
go mod download
```

Run server:

```sh
go run ./server/main.go
```

Run client:

```sh
go run ./client/main.go
```

## Installation (docker)

Build container:

```sh
docker build -t price_monitoring .
```

Run container: 

```sh
docker run -it -p 8081:8081 price_monitoring
```

## Documentation

Install protoc with go:
```sh
go get -u github.com/golang/protobuf/protoc-gen-go
```

You can generate client/server user protoc:
```sh
protoc -I proto proto/priceMonitoring.proto --go_out=plugins=grpc:proto/
```
