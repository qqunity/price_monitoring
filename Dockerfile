FROM golang:latest

ADD . /PriceMonitoring

WORKDIR /PriceMonitoring

RUN go mod download


ENTRYPOINT go run ./server/main.go

EXPOSE 8081