package controllers

import (
	"context"
	"log"
	"net/http"
	pb "priceMonitoring/proto"
	"priceMonitoring/server/storage"
	"time"
)

type PriceMonitoringServiceServer struct {}

type ThreadInfo struct {
	quitChan chan bool
	ticker *time.Ticker
}

var ThreadsInfo map[string]ThreadInfo


func (s *PriceMonitoringServiceServer) AddUrl(ctx context.Context,
	request *pb.AddUrlRequest) (*pb.AddUrlResponse, error) {
	log.Printf("Add url request with: url = %s, periodicity = %d", request.Url, request.Periodicity)
	var err error
	if request.Periodicity == 0 {
		request.Periodicity = 86400
	}
	result, err := storage.AddUrlInfo(request.Url, request.Periodicity)
	if err != nil {
		log.Println(err.Error())
		err = nil
	}
	response := new(pb.AddUrlResponse)

	if result {
		response.Status = true
		response.Error = ""
		ThreadsInfo[request.Url] = ThreadInfo{
			quitChan: make(chan bool),
			ticker: time.NewTicker(time.Duration(request.Periodicity) * time.Second),
		}
		go func() {
			for {
				select {
				case <-ThreadsInfo[request.Url].quitChan:
					ThreadsInfo[request.Url].ticker.Stop()
					return
				case <-ThreadsInfo[request.Url].ticker.C:
					response, err := http.Get(request.Url)
					if err != nil {
						log.Println(err.Error())
					}
					status, err := storage.AddUrlCheckInfo(request.Url, int64(response.StatusCode))
					ThreadsInfo[request.Url].ticker.Reset(time.Duration(request.Periodicity) * time.Second)
					if !status {
						log.Printf("Url does not exist")
					}
					if err != nil {
						log.Println(err.Error())
					}
					log.Printf("Add url check info with: url = %s, status_code = %d", request.Url, response.StatusCode)
				}
			}
		}()
	} else {
		response.Status = false
		response.Error = "This url already exists"
	}
	log.Printf("Add url response with: status = %v, error = %s", response.Status, response.Error)
	return response, err
}

func (s *PriceMonitoringServiceServer) GetUrlsCheckInfo(ctx context.Context,
	request *pb.GetUrlsCheckInfoRequest) (*pb.GetUrlsCheckInfoResponse, error) {
	log.Printf("Get urls check info request with: url = %s, limit = %d", request.Url, request.Limit)
	var err error
	if request.Limit == 0 {
		request.Limit = 5
	}
	data, err := storage.GetUrlsCheckInfo(request.Url, request.Limit)
	if err != nil {
		log.Println(err.Error())
		err = nil
	}
	response := new(pb.GetUrlsCheckInfoResponse)

	if data != nil {
		response.Status = true
		response.Error = ""
		for _, item := range data{
			response.Data = append(response.Data, &pb.GetUrlCheckInfoResponse{
				Date:       item.Date,
				StatusCode: int64(item.StatusCode),
			})
		}

	} else {
		response.Status = false
		response.Error = "There are no rows in query set"
		response.Data = nil
	}
	log.Printf("Get urls check info response with: status = %v, error = %s, data = %v", response.Status, response.Error, response.Data)
	return response, err
}

func (s *PriceMonitoringServiceServer) DisableUrlInfo(ctx context.Context,
	request *pb.DisableUrlInfoRequest) (*pb.DisableUrlInfoResponse, error) {
	log.Printf("Disable url info request with: url = %s", request.Url)
	var err error
	result, err := storage.DisableUrlInfo(request.Url)
	if err != nil {
		log.Println(err.Error())
		err = nil
	}
	response := new(pb.DisableUrlInfoResponse)

	if result {
		response.Status = true
		response.Error = ""
		ThreadsInfo[request.Url].quitChan <- true
	} else {
		response.Status = false
		response.Error = "Url not found"
	}
	log.Printf("Disable url info response with: status = %v, error = %s", response.Status, response.Error)
	return response, err
}

func (s *PriceMonitoringServiceServer) GetUrlsCheckInfoByDate(ctx context.Context,
	request *pb.GetUrlsCheckInfoByDateRequest) (*pb.GetUrlsCheckInfoByDateResponse, error) {
	log.Printf("Get urls check info by date request with: date = %s, n = %d", request.Date, request.N)
	var err error
	data, err := storage.GetUrlsCheckInfoByDate(request.Date, request.N)
	if err != nil {
		log.Println(err.Error())
		err = nil
	}
	response := new(pb.GetUrlsCheckInfoByDateResponse)

	if data != nil {
		response.Status = true
		response.Error = ""
		for _, item := range data {
			response.Urls = append(response.Urls, &pb.UrlRequest{
				Url: item.Url,
			})
		}
	} else {
		response.Status = false
		response.Error = "There are no rows in query set"
		response.Urls = nil
	}
	log.Printf("Get urls check info by date response with: status = %v, error = %s, urls = %v", response.Status, response.Error, response.Urls)
	return response, err
}