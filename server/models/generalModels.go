package models

type UrlsCheckInfo struct {
	StatusCode int    `json:"statusCode"`
	Date       string `json:"date"`
}

type Url struct {
	Url string `json:"url"`
}
