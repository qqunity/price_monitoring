package utils

import (
	"gopkg.in/yaml.v2"
	"log"
	"os"
	"priceMonitoring/server/models"
)

func GetConfig() *models.Config {
	cfg := new(models.Config)
	f, err := os.Open("server/config.yml")
	if err != nil {
		log.Println(err.Error())
	}
	defer f.Close()

	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(cfg)
	if err != nil {
		log.Println(err.Error())
	}
	return cfg
}