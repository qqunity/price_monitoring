package main

import (
	"fmt"
	"google.golang.org/grpc"
	"log"
	"net"
	pb "priceMonitoring/proto"
	"priceMonitoring/server/controllers"
	"priceMonitoring/server/storage"
	"priceMonitoring/server/utils"
)

func main() {
	storage.InitializeDB()
	server := grpc.NewServer()
	controllers.ThreadsInfo = make(map[string]controllers.ThreadInfo)

	instance := new(controllers.PriceMonitoringServiceServer)

	pb.RegisterPriceMonitoringServiceServer(server, instance)

	listener, err := net.Listen("tcp", fmt.Sprintf(":%s", utils.GetConfig().Server.Port))
	if err != nil {
		log.Println(err)
	}

	if err = server.Serve(listener); err != nil {
		log.Fatalf("Unable to start server: %s", err.Error())
	}
}