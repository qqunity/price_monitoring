package storage

import (
	"encoding/json"
	"priceMonitoring/server/models"
)

func AddUrlInfo(url string, periodicity int64) (bool, error) {
	query := `select * from public.app_url_add_url_info(url_in := $1, periodicity_in := $2);`
	var result bool
	err := DB.Get(&result, query, url, periodicity)
	return result, err
}

func AddUrlCheckInfo(url string, statusCode int64) (bool, error) {
	query := `select * from public.app_url_add_url_check_info(url_in := $1, status_code_in := $2);`
	var result bool
	err := DB.Get(&result, query, url, statusCode)
	return result, err
}

func GetUrlsCheckInfo(url string, limit int64)  ([]models.UrlsCheckInfo, error){
	var result []byte
	var data []models.UrlsCheckInfo
	query := `select * from public.app_url_get_urls_check_info(url_in := $1, limit_in := $2);`
	err := DB.Get(&result, query, url, limit)
	_ = json.Unmarshal(result, &data)
	return data, err
}

func DisableUrlInfo(url string) (bool, error) {
	query := `select * from public.app_url_disable_url_info(url_in := $1);`
	var result bool
	err := DB.Get(&result, query, url)
	return result, err
}

func GetUrlsCheckInfoByDate(date string, n int64) ([]models.Url, error)  {
	query := `select * from public.app_url_get_urls_check_info_by_date(date_in := $1, n_in := $2);`
	var result []byte
	var data []models.Url
	err := DB.Get(&result, query, date, n)
	_ = json.Unmarshal(result, &data)
	return data, err
}