package storage

import (
	"fmt"
	_ "github.com/jackc/pgx/v4/stdlib"
	"github.com/jmoiron/sqlx"
	"log"
	"priceMonitoring/server/utils"
)

var DB *sqlx.DB

func InitializeDB() {
	var err error
	cfg := utils.GetConfig()
	connectionUrl := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		cfg.Database.Host,
		cfg.Database.Port,
		cfg.Database.Username,
		cfg.Database.Password,
		cfg.Database.DBName)

	if DB, err = sqlx.Open("pgx", connectionUrl); err != nil {
		log.Printf("Cannot connect to \"%s\" database.\nError: %s\n", cfg.Database.DBName, err.Error())
		return
	}
	if err = DB.Ping(); err != nil {
		log.Printf("Cannot ping \"%s\" database\n", cfg.Database.DBName)
		return
	}
	log.Printf("Successful connection \"%s\" database\n", cfg.Database.DBName)
}
